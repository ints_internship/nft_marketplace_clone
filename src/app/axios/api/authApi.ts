import { ILogin, IRegister, IUserDetail } from 'types/common';
import { axiosClient } from '../axiosClient';

const authApi = {
  register: async (data: IRegister) => {
    const res = await axiosClient.post('auth/register', {
      name: data.name,
      email: data.email,
      password: data.password,
    });
    return res;
  },
  login: async (data: ILogin) => {
    const res = await axiosClient.post('auth/login', {
      email: data.email,
      password: data.password,
    });
    return res;
  },
};

export default authApi;
