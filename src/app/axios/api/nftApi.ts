import { IndexedObject } from 'types/common';
import { axiosClient } from '../axiosClient';
import { createQueryUrl } from 'app/helpers/functions';

const nftApi = {
  getAllNfts: async (queryParams: IndexedObject) => {
    const res = await axiosClient.get(createQueryUrl({ pathname: 'nfts' }, queryParams));
    return res;
  },
  getNftById: async (idNft: string) => {
    const res = await axiosClient.get(`nfts/${idNft}`);
    return res;
  },
};

export default nftApi;
