import { axiosClient } from '../axiosClient';

const commonApi = {
  getOverview: async () => {
    const res = await axiosClient.get('common/overview');
    return res;
  },
};

export default commonApi;
