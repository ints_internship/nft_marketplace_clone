import { IUserDetail } from 'types/common';
import { axiosClient } from '../axiosClient';

const accessToken = window.localStorage.getItem('access_token');
const refreshToken = window.localStorage.getItem('refresh_token');

const userApi = {
  getRankingList: async () => {
    const res = await axiosClient.get('users/ranking');
    return res;
  },
  getCurrentUser: async () => {
    const res = await axiosClient.get('users/detail');
    return res;
  },
  getUserById: async (id: string) => {
    const res = await axiosClient.get(`users/${id}`);
    return res;
  },
  updateUserDetail: async (data: IUserDetail) => {
    const res = await axiosClient.patch('users/detail', {
      name: data.name,
      email: data.email,
      avatar: data.avatar,
      banner: data.banner,
      bio: data.bio,
    });
    return res;
  },
};

export default userApi;
