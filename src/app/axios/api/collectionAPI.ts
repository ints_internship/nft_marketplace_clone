import { createQueryUrl } from 'app/helpers/functions';
import { axiosClient } from '../axiosClient';
import { IndexedObject } from 'types/common';

const collectionAPI = {
  getAllCollections: async (queryParams: IndexedObject) => {
    const res = axiosClient.get(createQueryUrl({ pathname: 'collectionNfts' }, queryParams));
    return res;
  },
};

export default collectionAPI;
