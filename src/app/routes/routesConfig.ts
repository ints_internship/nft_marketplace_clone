export const Epath = {
  // NotFoundPage
  notFoundPage: '/notfound',

  // Login Page
  loginPage: '/login',

  // Register Page
  registerPage: '/register',

  // Register Confirm Page
  registerConfirmPage: '/register-confirm',

  // Home Page
  homePage: '/',

  // Marketplace
  marketplace: '/marketplace',

  // Users Page
  users: '/users',

  // NFTs Page
  nfts: '/nfts',

  // Collections Page
  collections: '/collections',

  // Ranking Page
  ranking: '/ranking',
};
