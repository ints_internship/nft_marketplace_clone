import Logo from 'app/components/Logo/Logo';
import './Footer.scss';
import {
  DiscordLogoIcon,
  InstagramLogoIcon,
  TwitterLogoIcon,
  YoutubeLogoIcon,
} from 'app/components/Icons/Icons';
import SubscribeForm from 'app/components/Form/SubscribeForm/SubscribeForm';

function Footer() {
  return (
    <div className="footer">
      <div className="footer-info">
        <div className="marketplace-info">
          <Logo />
          <div className="additional-info">
            <p>NFT marketplace UI created with Anima for Figma.</p>
            <div className="community-info">
              <p>Join our community</p>
              <div className="icons-container">
                <DiscordLogoIcon />
                <YoutubeLogoIcon />
                <TwitterLogoIcon />
                <InstagramLogoIcon />
              </div>
            </div>
          </div>
        </div>
        <div className="explore">
          <h5 className="mono">Explore</h5>
          <div className="list-pages">
            <div>Marketplace</div>
            <div>Rankings</div>
            <div>Connect a wallet</div>
          </div>
        </div>
        <div className="subscribe">
          <h5 className="mono capitalize">Join our weeky digest</h5>
          <div className="subscribe-form">
            <p>Get exclusive promotions & updates straight to your inbox.</p>
            <SubscribeForm isSubscibe={true} />
          </div>
        </div>
      </div>
      <div className="copyright">
        <div className="divider"></div>
        <p>Ⓒ NFT Market. Use this template freely.</p>
      </div>
    </div>
  );
}

export default Footer;
