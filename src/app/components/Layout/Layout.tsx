import React, { ReactNode } from 'react';
import Header from './Header/Header';
import Footer from './Footer/Footer';

export type PropsLayout = {
  children: ReactNode;
};

function Layout({ children }: PropsLayout) {
  return (
    <div className="layout">
      <Header />
      <div>{children}</div>
      <Footer />
    </div>
  );
}

export default Layout;
