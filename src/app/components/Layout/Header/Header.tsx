import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { DEFAULT_LANGUAGE } from 'locales/i18n';
import { Epath } from 'app/routes/routesConfig';
import Logo from 'app/components/Logo/Logo';
import ButtonEvent from 'app/components/Button/Button';
import { BurgerMenuIcon, UserIcon } from 'app/components/Icons/Icons';
import { getUserById } from 'store/userReducer';
import { logOut } from 'store/authReducer';

function Header() {
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const history = useHistory();
  const currentUser = JSON.parse(window.localStorage.getItem('data_user') as string);

  const onLogOut = () => {
    dispatch(logOut());
    i18n.changeLanguage(DEFAULT_LANGUAGE);
    history.push(Epath.loginPage);
  };

  const onProfile = () => {
    dispatch(getUserById(currentUser.id));
    history.push(Epath.users);
  };

  return (
    <div className="header-navbar">
      <a href="/">
        <Logo />
      </a>
      <nav className="horizon-menu">
        <ul className="menu-list">
          <li className="menu-items e-hover">
            <a href={Epath.marketplace}>Marketplace</a>
          </li>
          <li className="menu-items e-hover">
            <a href={Epath.ranking}>Rankings</a>
          </li>
          <li className="menu-items e-hover">
            <a href="#">Connect a wallet</a>
          </li>
        </ul>
        {currentUser ? (
          <div className="group-btns-horizontal">
            <ButtonEvent onClick={onProfile} className="btn-tertiary" pr={20} pl={20}>
              Profile
            </ButtonEvent>
            <ButtonEvent onClick={onLogOut} className="btn-tertiary" pr={20} pl={20}>
              Logout
            </ButtonEvent>
          </div>
        ) : (
          <a className="e-hover" href="/register">
            <ButtonEvent className="btn-secondary filled" pr={30} pl={30}>
              <UserIcon size={20} />
              <p>Sign up</p>
            </ButtonEvent>
          </a>
        )}
      </nav>
      <div className="burger-menu">
        <BurgerMenuIcon size={24} />
      </div>
    </div>
  );
}

export default Header;
