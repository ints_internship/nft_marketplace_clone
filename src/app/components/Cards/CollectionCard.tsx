import './styles/CollectionCard.scss';
import { FC, useEffect, useState } from 'react';
import { ICollection, INft, IResponseNft } from 'types/common';
import nftApi from 'app/axios/api/nftApi';
import c from 'clsx';

interface Props {
  collections: ICollection;
}

const CollectionCard: FC<Props> = ({ collections }) => {
  const [nftCollection, setNftCollection] = useState<INft[]>([]);

  useEffect(() => {
    nftApi.getAllNfts({ collectionNft: collections.id }).then((res) => {
      const nftData = res.data as unknown as IResponseNft;
      setNftCollection(nftData.results.slice(0, 2));
    });
  }, [collections.id]);

  return (
    <div className="wrapper-collections">
      <div className="collection-thumbnail">
        <div className="photo-primary e-hover">
          <img src={collections.image} alt={collections.description} />
        </div>
        <div className="photo-nft">
          {nftCollection.map((nft) => (
            <a key={nft.id} className="e-hover" href="/collection">
              <img src={nft.image} alt={nft.name} />
            </a>
          ))}
          <a
            className={c(collections.quantityNfts > 1 ? '' : 'invisible', 'e-hover')}
            href="/collection"
          >
            <div className="additional-number">
              <h5 className="mono">{collections.quantityNfts - 2}+</h5>
            </div>
          </a>
        </div>
      </div>
      <div className="collection-info">
        <div className="collection-name">
          <h5>{collections.name}</h5>
        </div>
        <p>{collections.description}</p>
      </div>
    </div>
  );
};

export default CollectionCard;
