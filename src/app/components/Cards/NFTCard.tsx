import ArtistCard from './ArtistCard';
import c from 'clsx';
import './styles/NFTCard.scss';
import { FC } from 'react';
import { INft } from 'types/common';
import FormatDate from 'app/helpers/date';
import { DateFormatType } from 'types/dateTime';
import { DateReverseFormat } from 'utils/constant/dateTime.constant';

interface Props {
  type?: string | 'sm';
  bgColor?: string | 'bg' | 'bg-secondary';
  nftData: INft;
  onClick?: (e: React.MouseEvent<HTMLElement>) => void;
}

const NFTCard: FC<Props> = (props) => {
  const { type, nftData, bgColor, onClick } = props;

  return (
    <div
      onClick={onClick}
      className={c('wrapper-card nft-card e-hover', type && `nft-card-${type}`)}
    >
      <div className="image-card">
        <img src={nftData.image} alt={nftData.name} />
      </div>
      <div className={c('nft-info', type && `nft-info-${type}`, bgColor)}>
        <div className="artist-info nft-artist-card">
          <div className="nft-name">
            <h5>{nftData.name}</h5>
          </div>
          <ArtistCard className="p-0" type="small" artist={nftData.user} inherit mono />
        </div>
        <div className={c('additional-nft-info', type && `additional-nft-info-${type}`)}>
          <div className="price">
            <span className="mono caption">Price</span>
            <p className="mono">{nftData.price} ETH</p>
          </div>
          <div className="bid">
            <span className="mono caption capitalize">Created date</span>
            <p className="mono">
              {FormatDate({
                time: nftData.createdAt,
                format: `${DateReverseFormat}` as DateFormatType,
              })}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NFTCard;
