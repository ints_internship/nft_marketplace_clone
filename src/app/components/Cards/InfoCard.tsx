import StartEarningIcon from 'assets/image/StartEarningIcon.png';
import SetupWalletIcon from 'assets/image/SetupWalletIcon.png';
import CreateCollectionIcon from 'assets/image/CreateCollectionIcon.png';
import './styles/InfoCard.scss';

const InfoCard = () => {
  return (
    <div className="wrapper-card info-card">
      <div className="card-icon">
        <img src={StartEarningIcon} alt="Start Earning" />
      </div>
      <div className="card-details">
        <h5 className="capitalize">Setup Your wallet</h5>
        <p className="detail-card">
          Set up your wallet of choice. Connect it to the NFT market by clicking the wallet icon in
          the top right corner.
        </p>
      </div>
    </div>
  );
};
export default InfoCard;
