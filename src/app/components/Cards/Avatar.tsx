import './styles/Avatar.scss';
import avatar from '../../../assets/image/avatar.png';
import { FC } from 'react';
import c from 'clsx';

interface Props {
  className?: string;
  src: string;
  size: string | 'large' | 'medium' | 'small';
}

const AvatarCard: FC<Props> = (props) => {
  const { className, size, src = avatar } = props;
  return (
    <div className={c(`${size}`, className)}>
      <img className={c(`${size}`, 'img-avatar')} src={src} alt="avatar" />
    </div>
  );
};

export default AvatarCard;
