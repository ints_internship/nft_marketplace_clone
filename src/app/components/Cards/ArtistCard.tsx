import { FC } from 'react';
import c from 'clsx';
import AvatarCard from './Avatar';
import './styles/ArtistCard.scss';
import { IUserDetail } from 'types/common';

interface Props {
  className?: string;
  type?: string | 'default' | 'large' | 'medium' | 'small';
  inherit?: boolean;
  artist: IUserDetail;
  rankedNumber?: number;
  mono?: boolean;
  onClick?: (e: React.MouseEvent<HTMLElement>) => void;
}

const ArtistCard: FC<Props> = (props) => {
  const {
    className,
    type = 'default',
    inherit = false,
    artist,
    rankedNumber,
    onClick,
    mono,
  } = props;

  return (
    <div
      className={c(type === 'default' ? 'wrapper-card e-hover' : `wrapper-card-${type}`, className)}
      style={inherit ? { backgroundColor: 'inherit' } : { backgroundColor: '#3b3b3b' }}
      onClick={onClick}
    >
      <div className={c(type === 'default' ? 'artist-avatar' : `artist-avatar-${type}`)}>
        <AvatarCard
          src={artist.avatar}
          size={c(type === 'default' ? 'large' : type === 'large' ? 'medium' : 'small')}
        />
      </div>
      <div className={c(type === 'default' ? 'artist-info' : `artist-info-${type}`)}>
        <h5 className={c(mono ? 'mono' : '')}>{artist.name}</h5>
        <div className={c(type === 'default' ? 'additional-detail' : `additional-detail-${type}`)}>
          <div className="static-text">Total Sales:</div>
          <div className="mono">{artist.totalPrice}</div>
        </div>
      </div>
      <div className={c(type === 'default' ? 'ranking-number' : `ranking-number-${type}`)}>
        <p className="mono">{rankedNumber}</p>
      </div>
    </div>
  );
};
export default ArtistCard;
