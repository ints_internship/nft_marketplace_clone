import { FC } from 'react';
import './LabelTabs.scss';

interface ILabel {
  label: string;
  quantity: number;
}

const LabelTabs: FC<ILabel> = ({ label, quantity }) => {
  return (
    <div className="wrap-label">
      <h5>{label}</h5>
      <div className="quantity-number">
        <p className="mono">{quantity}</p>
      </div>
    </div>
  );
};

export default LabelTabs;
