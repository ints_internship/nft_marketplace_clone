import { FC, InputHTMLAttributes } from 'react';
import './SearchBar.scss';
import { SearchIcon } from '../Icons/Icons';
import { Input, InputProps } from 'antd';

const SearchBar: FC<InputProps> = ({ ...props }) => {
  return (
    <div className="input-form search">
      <Input {...props} />
      <SearchIcon size={24} />
    </div>
  );
};
export default SearchBar;
