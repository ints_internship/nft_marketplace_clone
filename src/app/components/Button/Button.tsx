import { ButtonHTMLAttributes, FC, ReactElement } from 'react';
import './Button.scss';
import c from 'clsx';

interface Props extends ButtonHTMLAttributes<HTMLButtonElement> {
  pt?: number;
  pr?: number;
  pb?: number;
  pl?: number;
  fw?: boolean;
}

const ButtonEvent: FC<Props> = ({ fw = false, pt = 0, pr = 50, pb = 0, pl = 50, ...props }) => {
  const paddingStyles: React.CSSProperties = {
    padding: `${pt}px ${pr}px ${pb}px ${pl}px`,
  };
  const { className, ...option } = props;

  return (
    <button
      className={c(fw && 'full-width', 'clickable', 'e-hover', className)}
      style={paddingStyles}
      {...option}
    ></button>
  );
};

export default ButtonEvent;
