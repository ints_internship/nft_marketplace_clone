import { FC, ReactNode } from 'react';
import './AuctionTimer.scss';
import c from 'clsx';

interface Props {
  children?: ReactNode;
  className?: string;
}

const AuctionTimer: FC<Props> = (props) => {
  const { className, children } = props;
  return (
    <div className={c('auction-timer', className)}>
      <p className="mono">Auction ends in: </p>
      <div className="timer">
        <div className="hours">
          <h3>59</h3>
          <p className="mono">Hours</p>
        </div>
        <h4>:</h4>
        <div className="minutes">
          <h3>59</h3>
          <p className="mono">Minutes</p>
        </div>
        <h4>:</h4>
        <div className="seconds">
          <h3>59</h3>
          <p className="mono">Seconds</p>
        </div>
      </div>
      {children}
    </div>
  );
};
export default AuctionTimer;
