import c from 'clsx';
import ButtonEvent from 'app/components/Button/Button';
import './SubscribeForm.scss';
import { FC, useEffect, useState } from 'react';
import { EnvelopeSimpleIcon } from 'app/components/Icons/Icons';

interface Props {
  isSubscibe?: boolean;
  isWidget?: boolean;
}

const SubscribeForm: FC<Props> = ({ isSubscibe, isWidget }) => {
  const [className, setClassName] = useState('btn-secondary filled');
  const [isMobile, setIsMobile] = useState(false);
  const [windowWidth, setWindowWidth] = useState<number>(window.innerWidth);

  useEffect(() => {
    const handleResize = () => {
      setWindowWidth(window.innerWidth);
    };
    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  useEffect(() => {
    isWidget
      ? windowWidth < 1280 && setClassName('btn-tertiary filled')
      : windowWidth < 834 && setClassName('btn-tertiary filled');
    setIsMobile(windowWidth < 834);
  }, [windowWidth, isWidget]);

  return (
    <form className={isWidget ? 'form-container-widget' : 'form-container'}>
      <input
        className={isWidget ? 'styled-inp-widget' : 'styled-inp'}
        placeholder="Enter your email here"
      />
      <ButtonEvent
        className={className}
        fw={(isSubscibe && isMobile) || (isWidget && windowWidth < 1280)}
      >
        <EnvelopeSimpleIcon className={c(isSubscibe && (isMobile ? '' : 'hidden'))} />
        <span>Subscribe</span>
      </ButtonEvent>
    </form>
  );
};

export default SubscribeForm;
