import { Form } from 'antd';
import authApi from 'app/axios/api/authApi';
import ButtonEvent from 'app/components/Button/Button';
import { EnvelopeSimpleIcon, LockKeyIcon, UserIcon } from 'app/components/Icons/Icons';
import InputForm from 'app/components/Input/InputForm';
import { ReactElement, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { registerAction } from 'store/authReducer';
import { IRegister } from 'types/common';

const RegisterForm = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [form] = Form.useForm<IRegister>();

  const compareToFirstPassword = (rule: any, value: string): Promise<void> => {
    if (value && value !== form.getFieldValue('password')) {
      return Promise.reject(new Error('The two passwords that you entered do not match!'));
    }
    return Promise.resolve();
  };

  const onFinish = async (values: IRegister) => {
    console.log('Received values of form: ', values);
    const { name, email, password } = values;
    dispatch(registerAction({ data: { name, email, password }, history }));
  };

  return (
    <Form
      form={form}
      name="register"
      className="register-form"
      onFinish={onFinish}
      scrollToFirstError
    >
      <div className="register-type-form">
        <Form.Item name="name" rules={[{ required: true, message: 'Please input your Username!' }]}>
          <InputForm icon={<UserIcon fill="svg-filled-2" size={20} />} placeholder="Username" />
        </Form.Item>
        <Form.Item
          name="email"
          rules={[
            { type: 'email', message: 'The input is not valid E-mail!' },
            { required: true, message: 'Please input your E-mail!' },
          ]}
        >
          <InputForm
            icon={<EnvelopeSimpleIcon fill="svg-filled-2" size={20} />}
            placeholder="Email Address"
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            { required: true, message: 'Please input your Password!' },
            { min: 8, message: 'Password must be at least 8 characters long!' },
          ]}
        >
          <InputForm
            icon={<LockKeyIcon fill="svg-filled-2" size={20} />}
            type="password"
            placeholder="Password"
          />
        </Form.Item>
        <Form.Item
          name="confirmPassword"
          dependencies={['password']}
          hasFeedback
          rules={[
            { required: true, message: 'Please confirm your password!' },
            { validator: compareToFirstPassword },
          ]}
        >
          <InputForm
            icon={<LockKeyIcon fill="svg-filled-2" size={20} />}
            type="password"
            placeholder="Confirm Password"
          />
        </Form.Item>
      </div>
      <ButtonEvent type="submit" className="btn-tertiary filled" fw>
        Create Account
      </ButtonEvent>
    </Form>
  );
};

export default RegisterForm;
