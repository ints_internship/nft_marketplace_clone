import { StoreFrontIcon } from '../Icons/Icons';

const Logo = () => {
  return (
    <div className="container-logo">
      <StoreFrontIcon />
      <p className="mono logo">NFT Marketplace</p>
    </div>
  );
};

export default Logo;
