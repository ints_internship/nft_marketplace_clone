import { FC, InputHTMLAttributes, ReactElement, useEffect, useState } from 'react';
import c from 'clsx';
import { EyeIcon, EyeSlashIcon } from '../Icons/Icons';

interface IInputFormProps extends InputHTMLAttributes<HTMLInputElement> {
  className?: string;
  icon?: ReactElement;
}

const InputForm: FC<IInputFormProps> = ({ className, icon, ...props }) => {
  const [isPassword, setIsPassword] = useState(false);
  const [showPassword, setShowPassword] = useState(false);

  useEffect(() => {
    setIsPassword(props.type === 'password');
  }, [props.type]);

  const toggleShowPassword = () => {
    setShowPassword(!showPassword);
  };
  return (
    <div className={c('input-form', className)}>
      {icon}
      <input {...props} />
      {isPassword &&
        (showPassword ? (
          <div id="icon-password" onClick={toggleShowPassword}>
            <EyeSlashIcon fill="svg-filled-2" size={20} />
          </div>
        ) : (
          <div id="icon-password" onClick={toggleShowPassword}>
            <EyeIcon fill="svg-filled-2" size={20} />
          </div>
        ))}
    </div>
  );
};
export default InputForm;
