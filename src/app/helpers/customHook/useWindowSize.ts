import { useEffect, useState } from 'react';
import { DESKTOP_SIZE, TABLET_SIZE } from 'utils/constant/windowSize.constant';

function useWindowSize() {
  const [windowWidth, setWindowWidth] = useState<number>(window.innerWidth);
  const [isMobile, setIsMobile] = useState<boolean>(false);
  const [isDesktop, setIsDesktop] = useState<boolean>(false);

  useEffect(() => {
    const handleResize = () => {
      setWindowWidth(window.innerWidth);
      setIsMobile(window.innerWidth < TABLET_SIZE);
      setIsDesktop(window.innerWidth >= DESKTOP_SIZE);
    };

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return { windowWidth, isMobile, isDesktop };
}

export default useWindowSize;
