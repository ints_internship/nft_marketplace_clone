import ButtonEvent from 'app/components/Button/Button';
import './Discover.scss';
import NFTCard from 'app/components/Cards/NFTCard';
import { EyeIcon } from 'app/components/Icons/Icons';
import { useEffect, useState } from 'react';
import { INft } from 'types/common';
import { useDispatch, useSelector } from 'react-redux';
import { getAllNft, getNftById } from 'store/nftReducer';
import { RootState } from 'types/RootState';
import { useHistory } from 'react-router-dom';
import { Epath } from 'app/routes/routesConfig';

const Discover = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [isMobile, setIsMobile] = useState(false);
  const [windowWidth, setWindowWidth] = useState<number>(window.innerWidth);
  const [discoverNfts, setDiscoverNfts] = useState<INft[]>([]);
  const listNfts = useSelector((state: RootState) => state.nft.listNfts);

  useEffect(() => {
    dispatch(getAllNft({ sortBy: 'createdAt:desc' }));
  }, [dispatch]);

  useEffect(() => {
    setDiscoverNfts(listNfts.slice(0, 3));
  }, [listNfts]);

  useEffect(() => {
    const handleResize = () => {
      setWindowWidth(window.innerWidth);
    };
    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  useEffect(() => {
    setIsMobile(windowWidth < 834);
  }, [windowWidth]);

  const onClickNft = (e: any, id: string) => {
    dispatch(getNftById(id));
    history.push(Epath.nfts);
  };

  return (
    <div className="container discover">
      <div className="discover-header">
        <div className="discover-headline capitalize">
          <div className="discover-heading">
            <h3>Discover more NFTs</h3>
          </div>
          <div className="discover-sub-head">
            <h5>Explore new treding NFTs</h5>
          </div>
        </div>
        {!isMobile && (
          <a href="/marketplace">
            <ButtonEvent className="btn-secondary">
              <EyeIcon fill="svg-filled-1" />
              <p className="capitalize">See all</p>
            </ButtonEvent>
          </a>
        )}
      </div>
      <div className="nft-list">
        {isMobile
          ? discoverNfts.map((nft) => (
              <NFTCard
                onClick={(e) => onClickNft(e, nft.id)}
                key={nft.id}
                type="sm"
                nftData={nft}
              />
            ))
          : discoverNfts.map((nft) => (
              <NFTCard onClick={(e) => onClickNft(e, nft.id)} key={nft.id} nftData={nft} />
            ))}
      </div>
      {isMobile && (
        <a href="/marketplace" className="mobile">
          <ButtonEvent className="btn-secondary" fw>
            <EyeIcon fill="svg-filled-1" />
            <p className="capitalize">See all</p>
          </ButtonEvent>
        </a>
      )}
    </div>
  );
};

export default Discover;
