import CollectionCard from 'app/components/Cards/CollectionCard';
import './TrendingCollections.scss';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import { ICollection } from 'types/common';
import { getAllCollections } from 'store/collectionReducer';
import { RootState } from 'types/RootState';
import { getCurrentUser } from 'store/authReducer';

const TrendingCollections = () => {
  const dispatch = useDispatch();
  const [displayCollections, setDisplayCollections] = useState<ICollection[]>([]);
  const listCollections = useSelector((state: RootState) => state.collection.listCollections);

  useEffect(() => {
    dispatch(getCurrentUser());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    dispatch(getAllCollections({ sortBy: 'view:desc' }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    setDisplayCollections(listCollections.slice(0, 3));
  }, [listCollections]);

  return (
    <div className="container trending-collection">
      <div className="tc-headline capitalize">
        <div className="tc-head">
          <h3>Trending Collection</h3>
        </div>
        <div className="tc-sub-head">
          <h5>Checkout our weekly updated trending collection.</h5>
        </div>
      </div>
      <div className="collections">
        {displayCollections.map((collection) => (
          <CollectionCard key={collection.id} collections={collection} />
        ))}
      </div>
    </div>
  );
};

export default TrendingCollections;
