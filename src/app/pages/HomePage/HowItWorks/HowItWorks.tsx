import InfoCard from 'app/components/Cards/InfoCard';
import './HowItWorks.scss';

const HowItWorks = () => {
  return (
    <div className="container hiw">
      <div className="hiw-header capitalize">
        <div className="hiw-heading">
          <h3>How it works</h3>
        </div>
        <div className="hiw-sub-head">
          <h5>Find out how to get started</h5>
        </div>
      </div>
      <div className="info-card-list">
        <InfoCard />
        <InfoCard />
        <InfoCard />
      </div>
    </div>
  );
};

export default HowItWorks;
