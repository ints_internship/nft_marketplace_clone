import { useEffect, useState } from 'react';
import ButtonEvent from '../../../components/Button/Button';
import { RocketLaunchIcon } from '../../../components/Icons/Icons';
import './HeroSection.scss';
import commonApi from 'app/axios/api/commonApi';
import { IOverview } from 'types/common';
import useWindowSize from '../../../helpers/customHook/useWindowSize';
import { initOverview } from 'utils/constant/initial.constant';
import { convertLongNumb } from 'app/helpers/common';

const HeroSection = () => {
  const { isMobile } = useWindowSize();
  const [overView, setOverview] = useState<IOverview>(initOverview);

  useEffect(() => {
    commonApi.getOverview().then((res) => {
      setOverview(res.data);
    });
  }, []);

  return (
    <div className="container">
      <div className="wrapper-hero">
        <div className="hero-content">
          <div className="hero-heading">
            <h1>Discover digital art & Collect NFTs</h1>
            <h5>
              NFT marketplace UI created with Anima for Figma. Collect, buy and sell art from more
              than 20k NFT artists.
            </h5>
          </div>
          {isMobile && (
            <a href="#">
              <div className="highlight-nft">
                <img
                  width="100%"
                  height="100%"
                  src="https://cdn.animaapp.com/projects/6357ce7c8a65b2f16659918c/files/heroanimationtransparentbck-2.gif"
                  alt="hightlight nft"
                />
              </div>
            </a>
          )}
          <a href="/login">
            <ButtonEvent className="btn-secondary filled" fw={isMobile}>
              <RocketLaunchIcon />
              <p>Get Started</p>
            </ButtonEvent>
          </a>
          <div className="hero-text">
            <div className="informations">
              <h4>{convertLongNumb(overView.totalSaleEth)}+</h4>
              <p>Total Sale</p>
            </div>
            <div className="informations">
              <h4>{convertLongNumb(overView.quantitySaleNfts)}+</h4>
              <p>Auctions</p>
            </div>
            <div className="informations">
              <h4>{convertLongNumb(overView.quantityArtists)}+</h4>
              <p>Artists</p>
            </div>
          </div>
        </div>
        {!isMobile && (
          <a href="#">
            <div className="highlight-nft">
              <img
                width="100%"
                height="100%"
                src="https://cdn.animaapp.com/projects/6357ce7c8a65b2f16659918c/files/heroanimationtransparentbck-2.gif"
                alt="hightlight nft"
              />
            </div>
          </a>
        )}
      </div>
    </div>
  );
};

export default HeroSection;
