import ButtonEvent from 'app/components/Button/Button';
import ArtistCard from 'app/components/Cards/ArtistCard';
import { RocketLaunchIcon } from 'app/components/Icons/Icons';
import './TopCreators.scss';
import { useEffect, useLayoutEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types/RootState';
import { getRankingList, getUserById } from 'store/userReducer';
import { IUserDetail } from 'types/common';
import userApi from 'app/axios/api/userApi';
import { useHistory } from 'react-router-dom';
import { Epath } from 'app/routes/routesConfig';

const TopCreators = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [isMobile, setIsMobile] = useState(false);
  const [isDesktop, setIsDesktop] = useState(false);
  const [windowWidth, setWindowWidth] = useState<number>(window.innerWidth);
  const listRankingUsers = useSelector((state: RootState) => state.user.listRanking);
  const [rankedList, setRankedList] = useState<IUserDetail[]>([]);

  useEffect(() => {
    dispatch(getRankingList());
  }, [dispatch]);

  useEffect(() => {
    const handleResize = () => {
      setWindowWidth(window.innerWidth);
    };
    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  useEffect(() => {
    setIsDesktop(windowWidth >= 1280);
    setIsMobile(windowWidth < 834);
  }, [windowWidth]);

  useEffect(() => {
    setRankedList(listRankingUsers.slice(0, 4));
  }, [listRankingUsers, isDesktop]);

  const onClickArtist = (e: any, id: string) => {
    dispatch(getUserById(id));
    history.push(Epath.users);
  };

  return (
    <div className="container top-creators">
      <div className="top-creators-header">
        <div className="top-creators-headline">
          <div className="top-creators-heading">
            <h3>Top creators</h3>
          </div>
          <div className="top-creators-sub-head">
            <h5>Checkout Top Rated Creators on the NFT Marketplace</h5>
          </div>
        </div>
        {!isMobile && (
          <ButtonEvent className="btn-secondary">
            <RocketLaunchIcon fill="svg-filled-1" />
            <p className="capitalize">View rankings</p>
          </ButtonEvent>
        )}
      </div>
      <div className="top-creators-list">
        {rankedList.map((artist) => (
          <ArtistCard
            onClick={(e) => onClickArtist(e, artist.id)}
            key={artist.id}
            type={isDesktop ? 'default' : 'large'}
            artist={artist}
            rankedNumber={rankedList.indexOf(artist) + 1}
          />
        ))}
      </div>
      {isMobile && (
        <a href="/rankings">
          <ButtonEvent className="btn-secondary" fw>
            <RocketLaunchIcon fill="svg-filled-1" />
            <p className="capitalize">View rankings</p>
          </ButtonEvent>
        </a>
      )}
    </div>
  );
};

export default TopCreators;
