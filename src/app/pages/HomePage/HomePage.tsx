import HeroSection from 'app/pages/HomePage/HeroSection/HeroSection';
import React from 'react';
import TrendingCollections from './TrendingCollections/TrendingCollections';
import TopCreators from './TopCreators/TopCreators';
import Discover from './Discover/Discover';
import Highlight from './Highlight/Highlight';
import HowItWorks from './HowItWorks/HowItWorks';
import Subscribe from './Subscribe/Subscribe';

type Props = {};

const HomePage = (props: Props) => {
  return (
    <>
      <HeroSection />
      <TrendingCollections />
      <TopCreators />
      <Discover />
      <Highlight />
      <HowItWorks />
      <Subscribe />
    </>
  );
};

export default HomePage;
