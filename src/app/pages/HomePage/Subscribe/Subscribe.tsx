import SubscribeForm from 'app/components/Form/SubscribeForm/SubscribeForm';
import SubscribePhoto from 'assets/image/subscribe-photo.png';
import './Subscribe.scss';

const Subscribe = () => {
  return (
    <div className="container subscribe-widget">
      <div className="wrapper-subscribe">
        <img src={SubscribePhoto} alt="astronaut" />
        <div>
          <div className="subscribe-header capitalize">
            <div className="subscribe-heading">
              <h3>Join our weekly digest</h3>
            </div>
            <div className="subscribe-sub-head">
              <h5>Get exclusive promotions & updates straight to your inbox.</h5>
            </div>
          </div>
          <SubscribeForm isWidget />
        </div>
      </div>
    </div>
  );
};
export default Subscribe;
