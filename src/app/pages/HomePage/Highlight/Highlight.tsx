import AuctionTimer from 'app/components/AuctionTimer/AuctionTimer';
import './Highlight.scss';
import ArtistCard from 'app/components/Cards/ArtistCard';
import ButtonEvent from 'app/components/Button/Button';
import { EyeIcon } from 'app/components/Icons/Icons';
import { useLayoutEffect, useState } from 'react';
import { INft, IResponseNft, IUserDetail } from 'types/common';
import nftApi from 'app/axios/api/nftApi';
import { useSelector } from 'react-redux';
import { RootState } from '../../../../types/RootState';
import useWindowSize from '../../../helpers/customHook/useWindowSize';

const initArtist: IUserDetail = {
  avatar: '',
  email: '',
  id: '',
  name: '',
};

const Highlight = () => {
  const [highlightNft, setHighlightNft] = useState<INft | null>(null);
  const { isMobile } = useWindowSize();

  useLayoutEffect(() => {
    nftApi.getAllNfts({ sortBy: 'view:desc' }).then((res) => {
      const data = res.data as unknown as IResponseNft;
      setHighlightNft(data.results[0]);
    });
  }, []);

  return (
    <div
      className="highlight"
      style={{ backgroundImage: `url(${highlightNft ? highlightNft.image : ''})` }}
    >
      <div className="highlight-content">
        <div>
          <div className="nft-highlight-info capitalize">
            <ArtistCard type="small" artist={highlightNft ? highlightNft.user : initArtist} />

            <h2>Magic Mashrooms</h2>
            {!isMobile && (
              <a href="#">
                <ButtonEvent className="btn-secondary filled-white" pt={22} pb={22}>
                  <EyeIcon fill="svg-filled-1" />
                  <p>See NFT</p>
                </ButtonEvent>
              </a>
            )}
          </div>
          <AuctionTimer className={isMobile ? 'full-width' : ''} />
          {isMobile && (
            <a className="full-width" href="#">
              <ButtonEvent className="btn-secondary filled-white full-width" pt={22} pb={22}>
                <EyeIcon fill="svg-filled-1" />
                <p>See NFT</p>
              </ButtonEvent>
            </a>
          )}
        </div>
      </div>
    </div>
  );
};

export default Highlight;
