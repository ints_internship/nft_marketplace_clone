import NFTCard from 'app/components/Cards/NFTCard';
import { Epath } from 'app/routes/routesConfig';
import { FC } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { getNftById } from 'store/nftReducer';
import { INft } from 'types/common';

interface Props {
  listNfts: INft[];
}

export const NFTs: FC<Props> = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { listNfts } = props;
  const onClickNft = (e: any, id: string) => {
    dispatch(getNftById(id));
    history.push(Epath.nfts);
  };
  return (
    <div className="container nfts-in-tabs">
      {listNfts.map((nfts) => (
        <NFTCard
          onClick={(e) => onClickNft(e, nfts.id)}
          key={nfts.id}
          nftData={nfts}
          bgColor="bg"
        />
      ))}
    </div>
  );
};
