import CollectionCard from 'app/components/Cards/CollectionCard';
import { FC } from 'react';
import { ICollection } from 'types/common';

interface Props {
  listCollections: ICollection[];
}
export const Collections: FC<Props> = (props) => {
  const { listCollections } = props;
  return (
    <div className="container collections-in-tabs">
      {listCollections.map((collections) => (
        <CollectionCard key={collections.id} collections={collections} />
      ))}
    </div>
  );
};
