import SearchBar from 'app/components/SearchBar/SearchBar';
import './Marketplace.scss';
import { Tabs } from 'antd';
import { ChangeEvent, EventHandler, FC, useEffect, useState } from 'react';
import { Collections } from './Collections';
import { NFTs } from './NFTs';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types/RootState';
import { getAllCollections } from 'store/collectionReducer';
import { getAllNft } from 'store/nftReducer';
import LabelTabs from 'app/components/Label/LabelTabs';

const Marketplace = () => {
  const dispatch = useDispatch();
  const { listNfts, paginationNFT } = useSelector((state: RootState) => state.nft);
  const { listCollections, paginationCollection } = useSelector(
    (state: RootState) => state.collection,
  );
  const [searchVal, setSearchVal] = useState<string>('');

  const onChangeVaule = (e: ChangeEvent<HTMLInputElement>) => {
    setSearchVal(e.target.value);
  };

  useEffect(() => {
    const getNftData = setTimeout(() => {
      dispatch(getAllNft(searchVal ? { name: searchVal } : {}));
    }, 1000);
    return () => clearTimeout(getNftData);
  }, [dispatch, searchVal]);

  useEffect(() => {
    dispatch(getAllCollections({}));
  }, [dispatch]);

  return (
    <div className="marketplace">
      <div className="container market-headline">
        <div>
          <div className="market-header">
            <h2 className="capitalize">Browse marketplace</h2>
            <h5>Browse through more than 50k NFTs on the NFT Marketplace.</h5>
          </div>
          <SearchBar
            placeholder="Search your favourite NFTs"
            allowClear
            style={{ width: '100%' }}
            onChange={onChangeVaule}
          />
        </div>
      </div>
      <div className="market-content">
        <Tabs
          defaultActiveKey="nfts"
          centered
          items={[
            {
              key: 'nfts',
              label: <LabelTabs label="NFTs" quantity={paginationNFT.totalResults} />,
              children: <NFTs listNfts={listNfts} />,
            },
            {
              key: 'collections',
              label: <LabelTabs label="Collections" quantity={paginationCollection.totalResults} />,
              children: <Collections listCollections={listCollections} />,
            },
          ]}
        />
      </div>
    </div>
  );
};

export default Marketplace;
