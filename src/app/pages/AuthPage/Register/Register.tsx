import RegisterForm from 'app/components/Form/RegisterForm/RegisterForm';
import RegisterPhoto from 'assets/image/register-img.png';
import './Register.scss';

const Register = () => {
  return (
    <div className="register-container">
      <img src={RegisterPhoto} alt="register" />
      <div className="create-account-form">
        <div className="register-header capitalize">
          <div className="register-heading">
            <h2>Create account</h2>
          </div>
          <div className="register-sub-head">
            <h5>Welcome! enter your details and start creating, collecting and selling NFTs.</h5>
          </div>
        </div>
        <RegisterForm />
        <p>
          Do you already have account? <a href="/login">Login</a>
        </p>
      </div>
    </div>
  );
};
export default Register;
