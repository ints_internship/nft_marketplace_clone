import React from 'react';
import { Form } from 'antd';
import InputForm from '../../../components/Input/InputForm';
import './Login.scss';
import ButtonEvent from 'app/components/Button/Button';
import { logInAction } from 'store/authReducer';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { ILogin } from 'types/common';

const LoginPage: React.FC = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const onSubmit = (data: ILogin) => {
    dispatch(logInAction({ data, history }));
  };

  return (
    <div className="container login">
      <div className="login-header">
        <div className="login-heading">
          <h3>Welcome to NFT Marketplace</h3>
        </div>
        <div className="login-sub-head">
          <h5>Login</h5>
        </div>
      </div>
      <div className="login-form">
        <Form autoComplete="off" onFinish={onSubmit}>
          <Form.Item name="email" rules={[{ required: true, message: 'Please input your email' }]}>
            <InputForm type="email" name="email" placeholder="Email Address" />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: 'Please input your password' }]}
          >
            <InputForm type="password" name="password" placeholder="Password" />
          </Form.Item>
          <Form.Item>
            <ButtonEvent className="btn-tertiary filled" fw type="submit">
              Login
            </ButtonEvent>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default LoginPage;
