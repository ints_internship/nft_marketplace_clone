import AuctionTimer from 'app/components/AuctionTimer/AuctionTimer';
import './NFTPage.scss';
import ArtistCard from 'app/components/Cards/ArtistCard';
import { ArrowRightIcon, GlobeIcon } from 'app/components/Icons/Icons';
import ButtonEvent from 'app/components/Button/Button';
import { useDispatch, useSelector } from 'react-redux';
import useWindowSize from 'app/helpers/customHook/useWindowSize';
import { RootState } from 'types/RootState';
import { getAllNft, getNftById } from 'store/nftReducer';
import { useEffect, useLayoutEffect, useState } from 'react';
import NFTCard from 'app/components/Cards/NFTCard';
import { getUserById } from 'store/userReducer';
import { useHistory } from 'react-router-dom';
import { Epath } from 'app/routes/routesConfig';
import { INft } from 'types/common';

const NFTPage = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { isMobile } = useWindowSize();
  const { currentNft, listNfts } = useSelector((state: RootState) => state.nft);
  const [listNFTsArtistPage, setListNFTsArtistPage] = useState<INft[]>([]);

  useEffect(() => {
    setListNFTsArtistPage(listNfts.filter((nft) => nft.id !== currentNft.id));
  }, [listNfts, currentNft]);
  useLayoutEffect(() => {
    currentNft.id && dispatch(getAllNft({ user: currentNft.user.id }));
  }, [currentNft, dispatch]);

  const onClickNft = (e: any, id: string) => {
    dispatch(getNftById(id));
  };

  const directToArtist = () => {
    dispatch(getUserById(currentNft.user.id));
    history.push(Epath.users);
  };

  return (
    <div className="nft-page">
      <div
        className="nft-cover"
        style={{
          backgroundImage: `url(${currentNft.image})`,
        }}
      ></div>
      <div id="nft-info">
        <div className="nft-info-section">
          <div>
            <div className="nft-details">
              <div id="headline">
                <h2>{currentNft.name}</h2>
                <h5 id="sub-head">Minted on Sep 30, 2022</h5>
              </div>
              {isMobile ? (
                <AuctionTimer>
                  <ButtonEvent className="btn-tertiary filled" fw>
                    Place Bid
                  </ButtonEvent>
                </AuctionTimer>
              ) : (
                ''
              )}
              <div id="main-info">
                <div id="created-by">
                  <h5 className="mono">Created By</h5>
                  <ArtistCard type="small" artist={currentNft.user} inherit />
                </div>
                <div id="description">
                  <h5 className="mono">Description</h5>
                  <p>{currentNft.description}</p>
                </div>
                <div id="details">
                  <h5 className="mono">Details</h5>
                  <div>
                    <GlobeIcon />
                    <h5>View on Etherscan</h5>
                  </div>
                  <div>
                    <GlobeIcon />
                    <h5>View Original</h5>
                  </div>
                </div>
                <div id="tags">
                  <h5 className="mono">Status</h5>
                  <div>{currentNft.status}</div>
                </div>
              </div>
            </div>
            {!isMobile ? (
              <AuctionTimer>
                <ButtonEvent className="btn-tertiary filled" fw>
                  Place Bid
                </ButtonEvent>
              </AuctionTimer>
            ) : (
              ''
            )}
          </div>
        </div>
      </div>
      <div className="container nft-related">
        <div className="related-headline capitalize">
          <h4>More from this artist</h4>
          {!isMobile ? (
            <ButtonEvent onClick={directToArtist} className="btn-secondary">
              <ArrowRightIcon fill="svg-filled-1" size={20} />
              <p>Go to artist page</p>
            </ButtonEvent>
          ) : (
            ''
          )}
        </div>
        <div id="related-section" className="nfts-in-tabs">
          {listNFTsArtistPage.map((nft) => (
            <NFTCard onClick={(e) => onClickNft(e, nft.id)} nftData={nft} bgColor="bg-secondary" />
          ))}
        </div>
        {isMobile ? (
          <ButtonEvent onClick={directToArtist} className="btn-secondary">
            <ArrowRightIcon fill="svg-filled-1" size={20} />
            <p>Go to artist page</p>
          </ButtonEvent>
        ) : (
          ''
        )}
      </div>
    </div>
  );
};

export default NFTPage;
