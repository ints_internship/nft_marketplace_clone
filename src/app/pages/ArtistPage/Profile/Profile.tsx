import { Form, Input, Tabs } from 'antd';
import ButtonEvent from 'app/components/Button/Button';
import {
  CopyIcon,
  EditIcon,
  GlobeIcon,
  DiscordLogoIcon,
  YoutubeLogoIcon,
  TwitterLogoIcon,
  InstagramLogoIcon,
} from 'app/components/Icons/Icons';
import { ellipsisText } from 'app/helpers/functions';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types/RootState';
import useWindowSize from 'app/helpers/customHook/useWindowSize';
import { useEffect, useState } from 'react';
import { updateUser } from 'store/userReducer';
import { IUserDetail } from 'types/common';
import { getCurrentUser } from 'store/authReducer';
import { convertLongNumb } from 'app/helpers/common';

const Profile = () => {
  const dispatch = useDispatch();
  const user = useSelector((state: RootState) => state.auth.currentUser);
  const { isDesktop } = useWindowSize();
  const [isEditing, setIsEditing] = useState(false);

  const submitEdited = (value: IUserDetail) => {
    dispatch(updateUser({ ...value, banner: user.banner }));
    setIsEditing(false);
  };

  return (
    <Form onFinish={submitEdited}>
      <div className="cover-image">
        <img src={user.banner} alt="banner" />
        <div className="wrap-profile-image">
          <div className="profile-image">
            <Form.Item>
              <img className="e-hover" src={user.avatar} alt="avatar" />
            </Form.Item>
          </div>
        </div>
      </div>
      <div className="user-info">
        <div className="artist-info-section">
          <div className="info-headline">
            <div className="artist-name">
              {isEditing ? (
                <Form.Item
                  className="input-form form-username"
                  name="name"
                  initialValue={user.name}
                >
                  <Input className="styled-inp" />
                </Form.Item>
              ) : (
                <h2>{user.name}</h2>
              )}
            </div>
            <div className="ctas">
              <ButtonEvent type="button" className="btn-secondary filled">
                <CopyIcon size={20} />
                <p>{ellipsisText(user.id)}</p>
              </ButtonEvent>
              <ButtonEvent
                type="button"
                onClick={() => {
                  setIsEditing(!isEditing);
                }}
                className="btn-secondary"
                pr={30}
                pl={30}
              >
                {isEditing ? (
                  <p>Cancel</p>
                ) : (
                  <>
                    <EditIcon size={20} />
                    <p>Edit Profile</p>
                  </>
                )}
              </ButtonEvent>
            </div>
          </div>
          <div className="info-text">
            <div className="artist-stats capitalize">
              <div className="stats">
                <h4 className="stats-numb mono">{convertLongNumb(1200000)}+</h4>
                <h5 className="stats-name">Volume</h5>
              </div>
              <div className="stats">
                <h4 className="mono stats-numb">{convertLongNumb(50)}+</h4>
                <h5 className="stats-name">NFTs sold</h5>
              </div>
              <div className="stats">
                <h4 className="mono stats-numb">{convertLongNumb(400000)}+</h4>
                <h5 className="stats-name">Followers</h5>
              </div>
            </div>
            <div className="artist-bio">
              <h5 className="mono bio">Bio</h5>
              {isEditing ? (
                <Form.Item className="input-form" name="bio" initialValue={user.bio}>
                  <Input className="styled-inp" />
                </Form.Item>
              ) : (
                <h5>{user.bio}</h5>
              )}
            </div>
            <div className="artist-links">
              <h5 className="mono">Links</h5>
              <div className="links-icons">
                <a className="e-hover" href="#">
                  <GlobeIcon size={isDesktop ? 32 : 24} />
                </a>
                <a className="e-hover" href="#">
                  <DiscordLogoIcon size={isDesktop ? 32 : 24} />
                </a>
                <a className="e-hover" href="#">
                  <YoutubeLogoIcon size={isDesktop ? 32 : 24} />
                </a>
                <a className="e-hover" href="#">
                  <TwitterLogoIcon size={isDesktop ? 32 : 24} />
                </a>
                <a className="e-hover" href="#">
                  <InstagramLogoIcon size={isDesktop ? 32 : 24} />
                </a>
              </div>
            </div>
          </div>
          {isEditing ? (
            <div className="group-btns-horizontal">
              <Form.Item>
                <ButtonEvent type="submit" className="btn-tertiary filled">
                  Save
                </ButtonEvent>
              </Form.Item>
            </div>
          ) : (
            ''
          )}
        </div>
      </div>
    </Form>
  );
};

export default Profile;
