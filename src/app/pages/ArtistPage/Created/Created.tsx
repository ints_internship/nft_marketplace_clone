import NFTCard from 'app/components/Cards/NFTCard';
import { Epath } from 'app/routes/routesConfig';
import { FC } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { getNftById } from 'store/nftReducer';
import { INft } from 'types/common';
import c from 'clsx';
import { PlusIcon } from 'app/components/Icons/Icons';
import { RootState } from 'types/RootState';
interface Props {
  listNfts: INft[];
  isProfile?: boolean;
}

export const Created: FC<Props> = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { listNfts, isProfile } = props;

  const onClickNFT = (e: any, idNFT: string) => {
    dispatch(getNftById(idNFT));
    history.push(Epath.nfts);
  };

  return (
    <div className="container nfts-in-tabs">
      {isProfile ? (
        <div className={c('wrapper-card nft-card e-hover additional-card')}>
          <PlusIcon fill="svg-none-fill" size={45} />
        </div>
      ) : null}
      {listNfts.map((nft) => (
        <NFTCard onClick={(e) => onClickNFT(e, nft.id)} nftData={nft} bgColor="bg" />
      ))}
    </div>
  );
};
