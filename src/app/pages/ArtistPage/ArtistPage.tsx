import ButtonEvent from 'app/components/Button/Button';
import {
  CopyIcon,
  DiscordLogoIcon,
  EditIcon,
  GlobeIcon,
  InstagramLogoIcon,
  PlusIcon,
  TwitterLogoIcon,
  YoutubeLogoIcon,
} from 'app/components/Icons/Icons';
import './ArtistPage.scss';
import { Tabs } from 'antd';
import { Created } from './Created/Created';
import { Owned } from './Owned/Owned';
import { Collection } from './Collection/Collection';
import LabelTabs from 'app/components/Label/LabelTabs';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useLayoutEffect, useState } from 'react';
import { getCurrentUser } from 'store/authReducer';
import { RootState } from 'types/RootState';
import { ellipsisText } from 'app/helpers/functions';
import { getAllNft } from 'store/nftReducer';
import useWindowSize from 'app/helpers/customHook/useWindowSize';
import { INft, IUserDetail } from 'types/common';
import Profile from './Profile/Profile';

const ArtistPage = () => {
  const dispatch = useDispatch();
  const { currentUser } = useSelector((state: RootState) => state.auth);
  const { currentArtist } = useSelector((state: RootState) => state.user);
  const { listNfts, paginationNFT } = useSelector((state: RootState) => state.nft);
  const { isDesktop } = useWindowSize();

  useLayoutEffect(() => {
    dispatch(getCurrentUser());
  }, []);

  useLayoutEffect(() => {
    const getNFT = setTimeout(() => {
      dispatch(getAllNft({ user: currentArtist.id, sortBy: 'createdAt:desc' }));
    }, 500);
    return () => {
      clearTimeout(getNFT);
    };
  }, [dispatch, currentArtist]);

  return (
    <div className="artist-page">
      {currentArtist.id === currentUser.id ? (
        <Profile />
      ) : (
        <>
          <div className="cover-image">
            <img id="banner" src={currentArtist.banner} alt="banner" />
            <div className="wrap-profile-image">
              <div className="profile-image">
                <img className="e-hover" src={currentArtist.avatar} alt="avatar" />
              </div>
            </div>
          </div>
          <div className="user-info">
            <div className="artist-info-section">
              <div className="info-headline">
                <div className="artist-name">
                  <h2>{currentArtist.name}</h2>
                </div>
                <div className="ctas">
                  <ButtonEvent className="btn-secondary filled">
                    <CopyIcon size={20} />
                    <p>{ellipsisText(currentArtist.id)}</p>
                  </ButtonEvent>
                  {currentArtist.id === currentUser.id ? (
                    <ButtonEvent className="btn-secondary" pr={30} pl={30}>
                      <EditIcon size={20} />
                      <p>Edit Profile</p>
                    </ButtonEvent>
                  ) : (
                    <ButtonEvent className="btn-secondary" pr={30} pl={30}>
                      <PlusIcon size={20} />
                      <p>Follow</p>
                    </ButtonEvent>
                  )}
                </div>
              </div>
              <div className="info-text">
                <div className="artist-stats capitalize">
                  <div className="stats">
                    <h4 className="stats-numb mono">1024+</h4>
                    <h5 className="stats-name">Volume</h5>
                  </div>
                  <div className="stats">
                    <h4 className="mono stats-numb">50+</h4>
                    <h5 className="stats-name">NFTs sold</h5>
                  </div>
                  <div className="stats">
                    <h4 className="mono stats-numb">3000+</h4>
                    <h5 className="stats-name">Followers</h5>
                  </div>
                </div>
                <div className="artist-bio capitalize">
                  <h5 className="mono bio">Bio</h5>
                  <h5>{currentArtist.bio}</h5>
                </div>
                <div className="artist-links">
                  <h5 className="mono">Links</h5>
                  <div className="links-icons">
                    <a className="e-hover" href="#">
                      <GlobeIcon size={isDesktop ? 32 : 24} />
                    </a>
                    <a className="e-hover" href="#">
                      <DiscordLogoIcon size={isDesktop ? 32 : 24} />
                    </a>
                    <a className="e-hover" href="#">
                      <YoutubeLogoIcon size={isDesktop ? 32 : 24} />
                    </a>
                    <a className="e-hover" href="#">
                      <TwitterLogoIcon size={isDesktop ? 32 : 24} />
                    </a>
                    <a className="e-hover" href="#">
                      <InstagramLogoIcon size={isDesktop ? 32 : 24} />
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      )}
      <Tabs
        centered
        items={[
          {
            key: 'created',
            label: <LabelTabs label={'Created'} quantity={paginationNFT.totalResults} />,
            children: (
              <Created isProfile={currentArtist.id === currentUser.id} listNfts={listNfts} />
            ),
          },
          {
            key: 'owned',
            label: <LabelTabs label={'Owned'} quantity={0} />,
            children: <Owned />,
          },
          {
            key: 'Collection',
            label: <LabelTabs label={'Collection'} quantity={0} />,
            children: <Collection />,
          },
        ]}
      />
    </div>
  );
};

export default ArtistPage;
