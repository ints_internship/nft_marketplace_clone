import { ICollection, INft, IOverview, IUserDetail } from 'types/common';

export const initUserDetail: IUserDetail = {
  id: '',
  name: '',
  email: '',
  avatar: '',
};

export const initCollection: ICollection = {
  id: '',
  name: '',
  description: '',
  view: 0,
  quantityNfts: 0,
  image: '',
};

export const initNftDetail: INft = {
  id: '',
  name: '',
  user: initUserDetail,
  collectionNft: initCollection,
  image: '',
  description: '',
  status: '',
  view: 0,
  price: 0,
};

export const initOverview: IOverview = {
  quantitySaleNfts: 0,
  quantityArtists: 0,
  totalSaleEth: 0,
};
