import { PayloadAction, createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import userApi from 'app/axios/api/userApi';
import { IUserDetail, IResRanking } from 'types/common';
import { initUserDetail } from 'utils/constant/initial.constant';

export interface UserState {
  listRanking: IUserDetail[];
  isLoading: boolean;
  currentArtist: IUserDetail;
}

const initState: UserState = {
  listRanking: [],
  isLoading: false,
  currentArtist: initUserDetail,
};

export const getRankingList = createAsyncThunk('users/ranking', async (_, { dispatch }) => {
  dispatch(setLoading(true));
  await userApi
    .getRankingList()
    .then((res) => {
      const rankingData = res.data as unknown as IResRanking;
      dispatch(setListRanking(rankingData.results));
      dispatch(setLoading(false));
    })
    .catch((err) => {
      alert(err.response.data.message);
      dispatch(setLoading(false));
    });
});

export const getUserById = createAsyncThunk(
  'users/detail',
  async (idUser: string | undefined, { dispatch }) => {
    dispatch(setLoading(true));
    await userApi
      .getUserById(idUser as string)
      .then((res) => {
        const userData = res.data as unknown as IUserDetail;
        dispatch(setCurrentArtist(userData));
      })
      .catch((err) => {
        alert(err.response.data.message);
      });
  },
);

export const updateUser = createAsyncThunk(
  'users/editDetail',
  async (data: IUserDetail, { dispatch }) => {
    await userApi
      .updateUserDetail(data)
      .then((res) => {
        const userData = res.data as unknown as IUserDetail;
        alert('Updated user details successfully!!!');
        setCurrentArtist(userData);
      })
      .catch((err) => {
        alert(`${err.response.data.message}. Update informations was failed!`);
      });
  },
);

const slice = createSlice({
  name: 'users',
  initialState: initState,
  reducers: {
    setListRanking(state, action: PayloadAction<IUserDetail[]>) {
      state.listRanking = action.payload;
    },
    setCurrentArtist(state, action: PayloadAction<IUserDetail>) {
      state.currentArtist = action.payload;
    },
    setLoading(state, action: PayloadAction<boolean>) {
      state.isLoading = action.payload;
    },
  },
});

export const { actions, reducer: userReducer } = slice;
export const { setListRanking, setLoading, setCurrentArtist } = actions;
export default userReducer;
