import { PayloadAction, createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import nftApi from 'app/axios/api/nftApi';
import { INft, IPagination, IResponseNft, IndexedObject } from 'types/common';
import { initNftDetail } from 'utils/constant/initial.constant';

export interface NFTState {
  listNfts: INft[];
  isLoading: boolean;
  paginationNFT: IPagination;
  currentNft: INft;
}

const initNftState: NFTState = {
  listNfts: [],
  isLoading: false,
  paginationNFT: {
    limit: 0,
    page: 0,
    totalPage: 0,
    totalResults: 0,
  },
  currentNft: initNftDetail,
};

export const getAllNft = createAsyncThunk(
  'nft/getAll',
  async (queryParams: IndexedObject, { dispatch }) => {
    dispatch(setLoading(true));
    await nftApi
      .getAllNfts(queryParams)
      .then((res) => {
        const nftData = res.data as unknown as IResponseNft;
        dispatch(setListNft(nftData.results));
        dispatch(setPagination({ ...nftData }));
        dispatch(setLoading(false));
      })
      .catch((err) => {
        alert(err);
        dispatch(setLoading(false));
      });
  },
);

export const getNftById = createAsyncThunk('nft/getById', async (id: string, { dispatch }) => {
  dispatch(setLoading(true));
  await nftApi
    .getNftById(id)
    .then((res) => {
      const nftData = res.data as unknown as INft;
      dispatch(setCurrentNft(nftData));
      dispatch(setLoading(false));
    })
    .catch((err) => {
      alert(err.response.data.message);
      dispatch(setLoading(false));
    });
});

const slice = createSlice({
  name: 'nft',
  initialState: initNftState,
  reducers: {
    setListNft(state, action: PayloadAction<INft[]>) {
      state.listNfts = action.payload;
    },
    setCurrentNft(state, action: PayloadAction<INft>) {
      state.currentNft = action.payload;
    },
    setLoading(state, action: PayloadAction<boolean>) {
      state.isLoading = action.payload;
    },
    setPagination(state, action: PayloadAction<IPagination>) {
      state.paginationNFT = action.payload;
    },
  },
});

export const { actions, reducer: nftReducer } = slice;
export const { setListNft, setLoading, setPagination, setCurrentNft } = actions;
export default nftReducer;
