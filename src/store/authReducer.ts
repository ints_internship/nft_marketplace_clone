import { PayloadAction, createAction, createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import authApi from 'app/axios/api/authApi';
import userApi from 'app/axios/api/userApi';
import {
  removeRefreshToken,
  removeToken,
  removeUser,
  saveRefreshToken,
  saveToken,
  saveUser,
} from 'app/helpers/localStorage';
import { Epath } from 'app/routes/routesConfig';
import { IResponseLogin, IRegister, IUserDetail, ILogin, TActionParams } from 'types/common';
import { initUserDetail } from 'utils/constant/initial.constant';

export interface AuthState {
  authenticated: boolean;
  loadingAuth: boolean;
  currentUser: IUserDetail;
}

const initialState: AuthState = {
  authenticated: false,
  loadingAuth: false,
  currentUser: initUserDetail,
};

export const getCurrentUser = createAsyncThunk('auth/getCurrentUser', async (_, { dispatch }) => {
  await userApi
    .getCurrentUser()
    .then((res) => {
      const dataUser = res.data as unknown as IUserDetail;
      dispatch(setCurrentUser(dataUser));
    })
    .catch((err) => {
      dispatch(logOut());
    });
});

export const registerAction = createAsyncThunk(
  'auth/registation',
  async (actionParams: TActionParams<IRegister>, { dispatch }) => {
    const { data, history } = actionParams;
    dispatch(setLoadingAuth(true));
    await authApi
      .register(data)
      .then((res) => {
        const userData = res.data as unknown as IResponseLogin;
        alert('Register Successfully!');
        dispatch(logIn(userData));
        dispatch(setLoadingAuth(false));
        history.push(Epath.homePage);
      })
      .catch((err) => {
        alert(`Something wrong: ${err.response.data.message}`);
        dispatch(setLoadingAuth(false));
      });
  },
);

export const logInAction = createAsyncThunk(
  'auth/login',
  async (actionParams: TActionParams<ILogin>, { dispatch, extra }) => {
    const { data, history } = actionParams;
    dispatch(setLoadingAuth(true));
    await authApi
      .login(data)
      .then((res) => {
        const userData = res.data as unknown as IResponseLogin;
        alert('Login Successfully!!!');
        dispatch(logIn(userData));
        dispatch(setLoadingAuth(false));
        history.push(Epath.homePage);
      })
      .catch((err) => {
        alert(`Login Unsuccessfully: ${err.response.data}`);

        dispatch(setLoadingAuth(false));
      });
  },
);

const slice = createSlice({
  name: 'auth',
  initialState: initialState,
  reducers: {
    setCurrentUser(state, action: PayloadAction<IUserDetail>) {
      state.currentUser = action.payload;
      saveUser(action.payload);
    },
    setLoadingAuth(state, action: PayloadAction<boolean>) {
      state.loadingAuth = action.payload;
    },
    logIn(state, action: PayloadAction<IResponseLogin>) {
      state.authenticated = true;
      state.currentUser = action.payload.user;
      saveToken(action.payload.tokens.access.token);
      saveRefreshToken(action.payload.tokens.refresh.token);
      saveUser(action.payload.user);
    },
    logOut(state) {
      state.authenticated = false;
      state.currentUser = initUserDetail;
      removeToken();
      removeRefreshToken();
      removeUser();
    },
  },
});

export const { actions, reducer: authReducer } = slice;
export const { setCurrentUser, setLoadingAuth, logIn, logOut } = actions;
export default authReducer;
