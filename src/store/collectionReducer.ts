import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import collectionAPI from 'app/axios/api/collectionAPI';
import {
  ICollection,
  IndexedObject,
  IPagination,
  IResponseCollection,
  TAction,
} from 'types/common';

export interface CollectionState {
  listCollections: ICollection[];
  isLoading: boolean;
  paginationCollection: IPagination;
}
const initCollectionReducer: CollectionState = {
  listCollections: [],
  isLoading: false,
  paginationCollection: {
    limit: 0,
    page: 0,
    totalPage: 0,
    totalResults: 0,
  },
};

export const getAllCollections = createAsyncThunk(
  'collections/getAll',
  async (queryParams: IndexedObject, { dispatch }) => {
    dispatch(setLoading(true));
    await collectionAPI
      .getAllCollections(queryParams)
      .then((res) => {
        const collectionsData = res.data as unknown as IResponseCollection;
        dispatch(setListCollections(collectionsData.results));
        dispatch(setPagination({ ...collectionsData }));
        dispatch(setLoading(false));
      })
      .catch((err) => {
        alert(`Something wrong. Please try again: ${err.response.data}`);
        dispatch(setLoading(false));
      });
  },
);

const slice = createSlice({
  name: 'collections',
  initialState: initCollectionReducer,
  reducers: {
    setListCollections(state, action: PayloadAction<ICollection[]>) {
      state.listCollections = action.payload;
    },
    setLoading(state, action: PayloadAction<boolean>) {
      state.isLoading = action.payload;
    },
    setPagination(state, action: PayloadAction<IPagination>) {
      state.paginationCollection = action.payload;
    },
  },
});

export const { actions, reducer: collectionReducer } = slice;
export const { setListCollections, setLoading, setPagination } = actions;
export default collectionReducer;
