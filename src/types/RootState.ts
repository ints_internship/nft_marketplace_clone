// [IMPORT NEW CONTAINERSTATE ABOVE] < Needed for generating containers seamlessly

import { AuthState } from 'store/authReducer';
import { CollectionState } from 'store/collectionReducer';
import { NFTState } from 'store/nftReducer';
import { UserState } from 'store/userReducer';

/*
  Because the redux-injectors injects your reducers asynchronously somewhere in your code
  You have to declare them here manually
*/
export interface RootState {
  auth: AuthState;
  collection: CollectionState;
  nft: NFTState;
  user: UserState;
}
