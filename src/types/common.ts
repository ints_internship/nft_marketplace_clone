export type IndexedObject<V = any> = { [k: string]: V };

export type TAction<T> = {
  type: string;
  payload: IndexedObject<T>;
};

export type Locale = { code: string; name: string; bidi: boolean };

export type TOption<T = string | number> = {
  label: string;
  value?: T;
};

export type TActionParams<T> = {
  data: T;
  history?: any;
};

export type TRefreshTokenRequest = {
  refreshToken: string;
};

export interface IPagination {
  page: number;
  limit: number;
  totalPage: number;
  totalResults: number;
}

export interface IToken {
  token: string;
  expires: string;
}

export interface ITokenResponse {
  access: IToken;
  refresh: IToken;
}

export interface ILogin {
  email: string;
  password: string;
}
export interface IRegister extends ILogin {
  name: string;
}

export interface IUserDetail {
  id: string;
  name: string;
  email: string;
  bio?: string;
  avatar: string;
  banner?: string;
  role?: string;
  eth?: number;
  isEmailVerified?: boolean;
  totalPrice?: number;
  countNft?: number;
}

export interface IResponseLogin {
  user: IUserDetail;
  tokens: ITokenResponse;
}

export interface ICollection {
  id: string;
  name: string;
  description: string;
  view: number;
  quantityNfts: number;
  image: string;
}

export interface IResponseCollection extends IPagination {
  results: ICollection[];
}

export interface INft {
  id: string;
  name: string;
  user: IUserDetail;
  collectionNft: ICollection;
  image: string;
  description: string;
  status: string;
  view: number;
  price: number;
  createdAt?: string;
  updatedAt?: string;
}

export interface IResponseNft extends IPagination {
  results: INft[];
}

export interface IOverview {
  quantitySaleNfts: number;
  quantityArtists: number;
  totalSaleEth: number;
}

export interface IResRanking extends IPagination {
  results: [];
}
